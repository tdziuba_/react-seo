// var express = require('express');
// var path = require('path');
// var httpProxy = require('http-proxy');
//
// var proxy = httpProxy.createProxyServer();
// var app = express();
//
// var isProduction = process.env.NODE_ENV === 'production';
// var port = isProduction ? process.env.PORT : 3000;
// var publicPath = path.resolve(__dirname, 'dist');
//
// //app.use(express.static(publicPath));
//
// // We only want to run the workflow when not in production
// if (!isProduction) {
//
//     // We require the bundler inside the if block because
//     // it is only needed in a development environment. Later
//     // you will see why this is a good idea
//     var bundle = require('./bundler.js');
//     bundle();
//
//     // Any requests to localhost:3000/build is proxied
//     // to webpack-dev-server
//     app.all('/build/*', function (req, res) {
//         proxy.web(req, res, {
//             target: 'http://localhost:8080'
//         });
//     });
//
// }
//
// // It is important to catch any errors from the proxy or the
// // server will crash. An example of this is connecting to the
// // server when webpack is bundling
// proxy.on('error', function(e) {
//     console.log('Could not connect to proxy, please try again...');
// });
//
// app.listen(port, function () {
//     console.log('Server running on port ' + port);
// });



var express          = require('express'),
    path = require('path'),
    app              = express(),
    webpack          = require('webpack'),
    WebpackDevServer = require('webpack-dev-server'),
    webpackConfig    = require('./webpack.config');

app.use(express.static( path.resolve(__dirname, 'dist') ));

// Serve index page
app.get('*', function ( req, res, next ) {
    res.sendFile(__dirname + '/index-dev.html');
});

//webpackConfig.plugins.push(new webpack.HotModuleReplacementPlugin());

// webpackConfig.entry.index = webpackConfig.entry.index.concat([
//     'webpack-dev-server/client?http://localhost:3000',
//     'webpack/hot/only-dev-server'
// ]);
//
// webpackConfig.entry.common = webpackConfig.entry.common.concat([
//     'webpack-dev-server/client?http://localhost:3000',
//     'webpack/hot/only-dev-server'
// ]);

var webpackServer = new WebpackDevServer(webpack(webpackConfig), {
    publicPath: webpackConfig.output.publicPath,
    hot: true,
    inline: true,
    historyApiFallback: true,
    stats: { colors: true }
});

webpackServer.listen(3000, 'localhost', function ( err ) {
    if ( err ) { console.log(err); }
});

var server = app.listen(80, function () {
    var host = 'localhost';
    var port = server.address().port;

    console.info(`React dev server listening at http://${host}:${port}`);
});