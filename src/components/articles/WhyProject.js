/**
 * Created by Citiboard AB Team
 * User: tdziuba <tomasz.dziuba@sprzedajemy.pl>
 * Date: 06.09.2016  15:24
 */

import React, {Component} from 'react'


class WhyProject extends Component {
	constructor(props) {
		super(props)
		this.parent = this.props.parent
	}

	render() {
		const {children} = this.props

		return (
			<div className="row columns">
				<h2>Po co ten projekt</h2>
				{ children }
			</div>
		)
	}
}

export default WhyProject
