/*
 * Copyright (C) 2015 Tomasz Dziuba <tomasz.dziuba@poczta.pl>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Created by Tomasz Dziuba <tomasz.dziuba@sprzedajemy.pl> on 30.08.2016.
 */
/// <reference path="../helpers/title-converter.ts" />

import React, { Component } from 'react'
import { Link } from 'react-router'
import { connect } from 'react-redux';
import axios from 'axios'
import * as action from '../actions/feed-actions'
import Spinner from '../components/common/Spinner'
import { titleConverter } from '../helpers/title-converter'

class Feed extends Component {
    constructor(props) {
        super(props);
        this.items = null;
    }

    componentDidMount() {
        let { dispatch, state } = this.props;
        let self = this;

        dispatch(action.getOffers(state));
        axios.get( '/jsons/offers_0.json' )
            .then( (response) => {
                dispatch(action.receiveOffers(state, response.data.data.offers))
                self.generateList();
            } )
            .catch( (error) => { console.warn(error) });
    }

    generateList () {
        let { state, feed } = this.props;

        if (feed) {
            this.items = feed.map((item) => {
                var title = titleConverter(item.title)
                return (
                    <div key={item.id} id={`offer_${item.id}`} className="offer">
                        <div className="main">
                            <h3 className="title"><Link to={`/item/${ title },${ item.id }`}>{ item.title }</Link></h3>
                            <figure className="picture"><Link to={`/item/${ title },${ item.id }`}><img src={ item.picture.full } alt={ item.title }/></Link></figure>
                        </div>
                    </div>
                )
            });

            this.forceUpdate();
        }

    }

    render() {
        return (
            <div className="reactFeed">
                <h3>Lista ofert</h3>

                <secion className="feedList">
                    { this.items || <Spinner/> }
                </secion>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        ...state
    }
}
export default connect(mapStateToProps, null, null, {
    pure: false
})(Feed)