import React, { Component } from 'react';
import ReactDom from 'react-dom';
import { Link } from 'react-router';
import Foundation from '../../helpers/foundation'

export class HeaderComponent extends Component {
    constructor (props) {
        super(props);
        this.menu = null;
        this.topBar = null;
    }

    componentDidMount() {
        var topBar = $('.top-bar');
        this.menu = new Foundation.DropdownMenu(topBar);
        this.topBar = new Foundation.Sticky(topBar, {
            stickyTo: 'top',
            stickyOn: 'xxlarge'
        });
        console.log(this.topBar)
    }

    render() {
        return (
            <div className="reactHeader" data-sticky-container>

                <div className="top-bar stacked-for-large stacked-for-medium"  data-sticky data-stick-to="top" data-margin-top="0">
                    <div className="top-bar-left">
                        <ul className="dropdown menu" data-dropdown-menu="wn9nts-dropdown-menu" role="menubar">
                            <li className="menu-text" role="menuitem"><Link to="/">React SEO Client-Side App</Link></li>
                            <li className="is-dropdown-submenu-parent">
                                <Link to="/o-projekcie">O projekcie</Link>
                                <ul className="menu vertical">
                                    <li><Link to="/dlaczego">Dlaczego</Link></li>
                                    <li><Link to="/zalozenia">Założenia</Link></li>
                                    <li><Link to="/srodowisko-testowe">Środowisko testowe</Link></li>
                                </ul>
                            </li>
                            <li role="menuitem"><Link to="/react-ajax-i-seo">React i ajax</Link></li>
                            <li role="menuitem"><Link to="/kod-zrodlowy">Kod żródłowy</Link></li>
                            <li role="menuitem"><Link to="/wyniki-testu">Wyniki</Link></li>
                        </ul>
                    </div>
                    <div className="top-bar-right">
                        <ul className="menu">
                            <li><input type="search" placeholder="Search" /></li>
                            <li><button type="button" className="button">Search</button></li>
                        </ul>
                    </div>
                </div>

            </div>
        )
    }
}