/**
 * Created by Citiboard AB Team
 * User: tdziuba <tomasz.dziuba@sprzedajemy.pl>
 * Date: 05.09.2016  11:23
 */

import React, { Component } from 'react'


class AboutProject extends Component {
	constructor(props) {
		super(props)
		this.parent = this.props.parent
	}

	render() {
		const { children } = this.props

		return (
			<article className="row columns aboutProject">
				<h2>O projekcie</h2>
				<div className="content">
					<p>Lorem ipsum reduktor sit amet, consectetur adipiscing elit. Integer et urna quis neque aliquam zapluty karzeł <strong>REACT'cji</strong> dictum quis ut massa. Mauris dolor sem, placerat at elit sed, varius auctor odio. Maecenas justo lectus, pharetra quis accumsan rhoncus, rhoncus quis ipsum. Sed vitae turpis felis. Duis eget ipsum vitae libero aliquam consequat. In ac egestas metus. Sed egestas ultrices metus, at tristique tellus faucibus ut.</p>
					<p>Proin interdum rutrum tellus nec <strong>client-side</strong>. Etiam dictum, ex vel aliquet tempus, augue arcu suscipit elit, ac tincidunt leo est ac nulla. Morbi volutpat pretium consequat. Ut commodo euismod neque, a vestibulum neque consectetur sed. Morbi dignissim aliquet nulla quis ullamcorper. Ut fermentum, eros ac congue malesuada, turpis mauris ullamcorper massa, in condimentum lectus risus nec nunc. Aenean congue est in nibh rutrum vestibulum. Vestibulum elementum mollis justo, aliquet rutrum mi ultricies id. Morbi aliquet imperdiet tellus. Cras iaculis justo sit amet elit eleifend lacinia. Proin ac placerat ante. Pellentesque at finibus odio. Etiam faucibus augue id nulla suscipit auctor. Nunc bibendum, metus vitae sagittis placerat, nibh dui scelerisque quam, non hendrerit nisl quam nec posuere.</p>
					<h3>Słowa kluczowe:</h3>
					<p>reduktor, <strong>zapluty karzeł REACT'cji</strong>, <strong>client-side</strong> rendering</p>
				</div>
			</article>

		)
	}
}

export default AboutProject
