/**
 * Created by Citiboard AB Team
 * User: tdziuba <tomasz.dziuba@sprzedajemy.pl>
 * Date: 07.09.2016  10:57
 */

import React, {Component} from 'react'


class Spinner extends Component {
	constructor(props) {
		super(props)
		this.parent = this.props.parent
	}

	render() {

		return (
			<div className="loading">
				<span>Zasysanie interneta...</span>
			</div>
		)
	}
}

export default Spinner
