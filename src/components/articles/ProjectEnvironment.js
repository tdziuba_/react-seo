/**
 * Created by Citiboard AB Team
 * User: tdziuba <tomasz.dziuba@sprzedajemy.pl>
 * Date: 06.09.2016  15:38
 */

import React, {Component} from 'react'


class ProjectEnvironment extends Component {
	constructor(props) {
		super(props)
		this.parent = this.props.parent
	}

	render() {
		const {children} = this.props

		return (
			<div className="row columns">
				<h2>Środowisko testowe</h2>
				{ children }
			</div>
		)
	}
}

export default ProjectEnvironment
