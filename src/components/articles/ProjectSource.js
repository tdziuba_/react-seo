/**
 * Created by Citiboard AB Team
 * User: tdziuba <tomasz.dziuba@sprzedajemy.pl>
 * Date: 07.09.2016  09:03
 */

import React, {Component} from 'react'


class ProjectSource extends Component {
	constructor(props) {
		super(props)
		this.parent = this.props.parent
	}

	render() {
		const {children} = this.props

		return (
			<div className="row columns">
				<h2>Kod żródłowy projektu</h2>
				{ children }
			</div>
		)
	}
}

export default ProjectSource
