/**
 * Created by tdziuba on 29.08.2016.
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Feed } from './Feed';
import { HeaderComponent } from '../components/header/HeaderComponent';

if (process.brower) {
	require('./../scss/index.scss');
}

class FrontExpApp extends Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {

    }

	render() {
		let { children } = this.props
		return (
			<div>
				<HeaderComponent/>
				{ children }
			</div>
		);
	}

}


function mapStateToProps(state) {
	return {
		...state
	}
}
export default connect(mapStateToProps, null, null, {
	pure: false
})(FrontExpApp)