/**
 * Created by tdziuba on 02.09.2016.
 */

import { GET_OFFERS, GET_MORE_OFFERS, RECEIVE_OFFERS } from '../action-constants';

export function getOffers(state, action) {
	return {
		type: GET_OFFERS,
		true
	}
}

export function receiveOffers(state, offers) {
	return {
		type: RECEIVE_OFFERS,
		offers,
		false
	}
}

export function getMoreOffers(state, offers) {
	return {
		type: GET_MORE_OFFERS,
		offers,
		true
	}
}