/**
 * Created by tdziuba on 05.09.2016.
 */

/*
import React, { Component } from 'react';
import ReactDOM, { render } from 'react-dom';
import { Provider } from 'react-redux'
import { Router, browserHistory } from 'react-router'
import { syncHistoryWithStore } from 'react-router-redux'
import { configureStore } from './store';
import { routes } from './routes'

let initialState = (typeof window !== 'undefined' && window.hasOwnProperty('initialState')) ? window.initialState : {}
const store = configureStore(browserHistory, initialState)
// const history = syncHistoryWithStore(browserHistory, store)
// console.log(history);
// render(
// 	<Provider store={store}>
// 		<Router history={history} routes={routes} store={store} />
// 	</Provider>,
// 	document.getElementById('react-app')
// )

Router.run(routes, Router.HistoryLocation, function(Handler, state) {
	const history = syncHistoryWithStore(state, store)
	React.render(
		<Provider store={store}>
			<Router history={history} routes={routes} store={store} />
		</Provider>,
		document.getElementById('react-app')
	)
})
	*/

/// <reference path="../node_modules/react-router" />
import React = require('react');
import { render } from 'react-dom'
import { Router, Route, IndexRoute, Link, browserHistory } from 'react-router'
import { Provider } from 'react-redux'
import { syncHistoryWithStore } from 'react-router-redux'
import { configureStore } from './store';
import routes from './routes'
// import * as c from './containers';
//
// let initialState = (typeof window !== 'undefined' && window.hasOwnProperty('initialState')) ? window.initialState : {}
// const store = configureStore(browserHistory, initialState)
// const history = syncHistoryWithStore(browserHistory, store)
//
// render((
// 	<Provider store={store}>
// 		<Router store={store} history={history} routes={routes}/>
// 	</Provider>
// ), document.getElementById('react-app'))