/**
 * Created by tdziuba on 07.09.2016.
 */

export const GET_ITEM_DETAILS = 'GET_ITEM_DETAILS';
export const RECEIVE_ITEM_DETAILS = 'RECEIVE_ITEM_DETAILS';