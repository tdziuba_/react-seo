/**
 * Created by tdziuba on 02.09.2016.
 */

export const GET_OFFERS = 'GET_OFFERS';
export const GET_MORE_OFFERS = 'GET_MORE_OFFERS';
export const RECEIVE_OFFERS = 'RECEIVE_OFFERS';