/**
 * Created by tdziuba on 29.08.2016.
 */

var path = require('path')
var webpack = require('webpack'),
	CompressionPlugin = require("compression-webpack-plugin"),
	ProgressBarPlugin = require('progress-bar-webpack-plugin'),
	ExtractTextPlugin = require("extract-text-webpack-plugin"),
	purify = require("purifycss-webpack-plugin"),
	sassLoaderPaths = [path.resolve(__dirname, "./scss")];

//sassLoaderPaths = sassLoaderPaths.concat(require('node-neat').includePaths);

module.exports = {
	devtool: 'source-map',
	entry: {
		index: './src/index',
		common: ['react', 'react-dom', 'react-router', 'react-redux', 'axios', './src/helpers', './src/reducers', 'hammerjs'],
		//commonCSS: ['./src/components/spinner/spinner.scss', './src/components/header/scss/_header.scss']
	},
	output: {
		path: path.join(__dirname, 'dist'),
		filename: 'js/[name].js',
		chunkFilename: 'js/[name].chunk.js',
		publicPath: 'http://frontexp.cba.pl/'
	},
	externals: {
		jQuery: 'jQuery',
		foundation: 'Foundation'
	},
	sassLoader: {
		includePaths: sassLoaderPaths
	},
	plugins: [
		new webpack.DefinePlugin({
			'process.env': {
				'NODE_ENV': JSON.stringify('production')
			}
		}),
		new webpack.ProvidePlugin({
			$: 'jquery',
			jQuery: 'jquery',
			'window.jQuery': 'jquery'
		}),
		new ProgressBarPlugin(),
		new webpack.optimize.OccurenceOrderPlugin(),
		new webpack.optimize.DedupePlugin(),
		//new webpack.optimize.MinChunkSizePlugin({minChunkSize: 600}),
		new webpack.optimize.CommonsChunkPlugin({
			names: ["common"],
			children: true,
			minChunks: 2
		}),
		new ExtractTextPlugin("css/[name].css", {
			allChunks: true,
			disable: false
		}),
		// new purify({
		// 	basePath: path.resolve(__dirname, 'src')
		// }),
		// new webpack.optimize.AggressiveMergingPlugin({
		// 	moveToParents: true
		// }),
		// new webpack.optimize.LimitChunkCountPlugin({
		// 	maxChunks: 15,
		// 	chunkOverhead: 20000,
		// 	entryChunkMultiplicator: 12
		// }),
		//new webpack.optimize.UglifyJsPlugin(),
		new CompressionPlugin({
			asset: "[path].gz[query]",
			algorithm: "gzip",
			test: /\.js$|\.css$|\.html$/,
			threshold: 10240,
			minRatio: 0.8
		})
	],
	resolveLoader: {
		alias: {
			"react-proxy": path.join(__dirname, "./src")
		}
	},
	resolve: {
		extensions: ["", ".jsx", ".js", ".scss"]
	},
	module: {
		loaders: [
			{ test: /\.scss$/, loader: ExtractTextPlugin.extract("style-loader", "css?sourceMap!sass-loader?sourceMap") },
			{
				test: /\.js$/,
				loaders: [ 'babel-loader' ],
				exclude: /node_modules/,
				include: __dirname
			},
			{
				test: /masonry|imagesloaded|fizzy\-ui\-utils|desandro\-|outlayer|get\-size|doc\-ready|eventie|eventemitter/,
				loader: 'imports?define=>false&this=>window'
			},
			{
				test: /\.json$/,
				loaders: [ 'json' ],
				exclude: /node_modules/,
				include: __dirname
			},

			{ test: /\.woff$/, loader: "url-loader?prefix=font/&limit=5000&mimetype=application/font-woff" },
			{ test: /\.ttf$/,  loader: "file-loader?prefix=font/" },
			{ test: /\.eot$/,  loader: "file-loader?prefix=font/" },
			{ test: /\.svg$/,  loader: "file-loader?prefix=font/" },

			//img
			{ test: /\.(png|jpg)$/, loader: 'file-loader?name=img/[name].[ext]' }
		]
	}
}
