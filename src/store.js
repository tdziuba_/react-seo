/**
 * Created by tdziuba on 02.09.2016.
 */
import React from 'react'

import { createStore, combineReducers, compose, applyMiddleware } from 'redux'
//import createLogger from 'redux-logger'
//import { createDevTools } from 'redux-devtools'
//import LogMonitor from 'redux-devtools-log-monitor'
//import DockMonitor from 'redux-devtools-dock-monitor'

import { routerReducer, routerMiddleware } from 'react-router-redux'

import * as reducers from './reducers'

// export const DevTools = createDevTools(
// 	<DockMonitor toggleVisibilityKey="ctrl-h" changePositionKey="ctrl-q">
// 		<LogMonitor theme="tomorrow" preserveScrollTop={false} />
// 	</DockMonitor>
// )

export function configureStore(history, initialState) {
	const reducer = combineReducers({
		...reducers,
		routing: routerReducer
	})

	const store = createStore(
		reducer,
		initialState,
		compose(
			applyMiddleware(
				routerMiddleware(history)
			),
			typeof window === 'object' && typeof window.devToolsExtension !== 'undefined' ? window.devToolsExtension() : f => f
		)
	)

	return store
}



//
// import { createStore, applyMiddleware, compose } from 'redux';
// import reducer from './reducers';
//
// let store = createStore(reducer, window.STATE_FROM_SERVER);
//
// export default store;
