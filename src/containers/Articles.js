/**
 * Created by Citiboard AB Team
 * User: tdziuba <tomasz.dziuba@sprzedajemy.pl>
 * Date: 05.09.2016  08:48
 */

import React, { Component } from 'react'
import { connect } from 'react-redux';
import axios from 'axios';
import AboutProject from '../components/articles/AboutProject'
import ProjectPrinciples from '../components/articles/ProjectPrinciples'
import ProjectEnvironment from '../components/articles/ProjectEnvironment'
import ProjectResults from '../components/articles/ProjectResults'
import ProjectSource from '../components/articles/ProjectSource'
import WhyProject from '../components/articles/WhyProject'
import AjaxSeo from '../components/articles/AjaxSeo'


class Articles extends Component {
	constructor(props) {
		super(props)
		this.parent = this.props.parent;
	}

	componentDidMount() {

	}

	getArticle() {
		let { dispatch, state, params, children } = this.props

		switch (params.title) {
			case 'o-projekcie':
				return <AboutProject />
			case 'zalozenia':
				return <ProjectPrinciples />
			case 'dlaczego':
				return <WhyProject />
			case 'srodowisko-testowe':
				return <ProjectEnvironment />
			case 'kod-zrodlowy':
				return <ProjectSource />
			case 'wyniki-testu':
				return <ProjectResults />
			case 'react-ajax-i-seo':
				return <AjaxSeo />
			default:
				return <AboutProject />
		}
	}

	render() {
		let { dispatch, state, params, children } = this.props

		return (
			<div className="row columns align-right articleContent">{ this.getArticle() }</div>
		)

	}
}

function mapStateToProps(state) {
	return {
		...state
	}
}
export default connect(mapStateToProps, null, null, {
	pure: false
})(Articles)