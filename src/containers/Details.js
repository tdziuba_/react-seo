/**
 * Created by Citiboard AB Team
 * User: tdziuba <tomasz.dziuba@sprzedajemy.pl>
 * Date: 07.09.2016  14:31
 */

import React, { Component } from 'react'
import { connect } from 'react-redux';
import axios from 'axios';
import * as action from '../actions/details-actions'

class Details extends Component {
	constructor(props) {
		super(props)
		this.parent = this.props.parent
	}

	componentDidMount() {
		let { dispatch, state } = this.props

		dispatch(action.getItemDetails(state))
	}

	render() {
		const {children} = this.props

		return (
			<div className="row columns offerDetails">
				<h2>Offer title</h2>
				{ children }
			</div>
		)
	}
}

function mapStateToProps(state) {
	return {
		...state
	}
}
export default connect(mapStateToProps, null, null, {
	pure: false
})(Details)