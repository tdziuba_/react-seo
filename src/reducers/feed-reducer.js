/**
 * Created by tdziuba on 02.09.2016.
 */
import * as types from '../action-constants/feed-action-constants'

let initialState = []

const feed = (state = initialState, action) => {

	if(action.type === types.GET_OFFERS) {
		return { list: action.offers, isFetching: action.isFetching }
	}
	else if(action.type === types.RECEIVE_OFFERS) {
		return action.offers
	}
	else if(action.type === types.REQ_ALL_PRODUCTS) {
		return { products: action.products, isFetching: action.isFetching }
	}
	//details
	else if(action.type === types.REQ_PRODUCT_DETAILS) {
		return { product: {}, isFetching: action.isFetching }
	}
	else if(action.type === types.RECV_PRODUCT_DETAILS) {
		return { product: action.product, isFetching: action.isFetching }
	}
	else if(action.type === types.ADD_PRODUCT) {
		return action.product
	}
	else if(action.type === types.EDIT_PRODUCT) {
		return action.product
	}

	return state
}

export default feed;