/**
 * Created by tdziuba on 05.09.2016.
 */
import React from 'react'
import { Route, IndexRoute } from 'react-router'
import * as c from './containers'

 let routes = [
	 <Route path="/" component={c.FrontExpApp}>
		 <IndexRoute component={c.Articles}/>
		 <Route path="article" component={c.Articles}>
			 <Route path="/:title" component={c.Articles}/>
		 </Route>
		 <Route path="/item/:title,:id" component={c.Details}/>
	 </Route>
 ]

export default routes