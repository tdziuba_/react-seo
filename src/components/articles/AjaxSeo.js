/**
 * Created by Citiboard AB Team
 * User: tdziuba <tomasz.dziuba@sprzedajemy.pl>
 * Date: 07.09.2016  09:09
 */

import React, {Component} from 'react'
import Feed from '../../containers/Feed'


class AjaxSeo extends Component {
	constructor(props) {
		super(props)
		this.parent = this.props.parent
	}

	render() {
		const {children} = this.props

		return (
			<div className="row columns">
				<h2>Ajax i SEO</h2>
				<p>Poniżej jest lista testowych ofert zaciaganych ajaxem i wyświetlanych po przetworzeniu jsona</p>
				{ children }
				<Feed />
			</div>
		)
	}
}

export default AjaxSeo
