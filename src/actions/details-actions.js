/**
 * Created by tdziuba on 07.09.2016.
 */

import { GET_ITEM_DETAILS, RECEIVE_ITEM_DETAILS } from '../action-constants/details-action-contants'

export function getItemDetails(action) {
	return {
		type: GET_ITEM_DETAILS,
		details: {},
		fetching: true
	}
}

export function receiveItemDetails(action, details) {
	return {
		type: RECEIVE_ITEM_DETAILS,
		details: details,
		fetching: false
	}
}