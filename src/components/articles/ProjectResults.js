/**
 * Created by Citiboard AB Team
 * User: tdziuba <tomasz.dziuba@sprzedajemy.pl>
 * Date: 07.09.2016  09:01
 */

import React, {Component} from 'react'


class ProjectResults extends Component {
	constructor(props) {
		super(props)
		this.parent = this.props.parent
	}

	render() {
		const {children} = this.props

		return (
			<div className="row columns">
				<h2>Wyniki testu</h2>
				{ children }
			</div>
		)
	}
}

export default ProjectResults
